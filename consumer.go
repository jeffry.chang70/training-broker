package main

import (
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
)

func FailOnErrorC(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main (){
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	FailOnErrorC(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	FailOnErrorC(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"payment",   // name
		"topic", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	FailOnErrorC(err, "Failed to declare an exchange")

	q, err := ch.QueueDeclare(
		"transfer", // name
		true,   // durable
		false,   // delete when unused
		false,   // exclusive
		false,   // no-wait
		nil,     // arguments
	)
	FailOnErrorC(err, "Failed to declare a queue")

	err = ch.QueueBind(
		q.Name, // queue name
		"*.transfer",     // routing key
		"payment", // exchange
		false,
		nil,
	)
	FailOnErrorC(err, "Failed to bind a queue")

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	var forever chan struct{}

	go func() {
		for d := range msgs {
			log.Printf("Received a message: %s", d.Body)
			err = ch.Publish(
				"",          // exchange
				d.ReplyTo, // routing key
				false,              // mandatory
				false,              // immediate
				amqp.Publishing{
					Headers:         nil,
					ContentType:     "text/plain",
					CorrelationId:   d.CorrelationId,
					Body:            []byte("APA JENG"),
				})
		}
	}()

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")
	<-forever




}