package Dto

type ReqPG struct {
	AccountNo       string `json:"account_no"`
	Amount          int    `json:"amount"`
	SourceAccountNo string `json:"source_account_no"`
}
