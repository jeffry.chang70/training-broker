package main

import (
	"encoding/json"
	amqp "github.com/rabbitmq/amqp091-go"
	"log"
	"training-broker/Dto"
)

func FailOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	FailOnError(err, "Failed to connect to RabbitMQ")
	defer conn.Close()

	ch, err := conn.Channel()
	FailOnError(err, "Failed to open a channel")
	defer ch.Close()

	err = ch.ExchangeDeclare(
		"payment", // name
		"topic",   // type
		true,      // durable
		false,     // auto-deleted
		false,     // internal
		false,     // no-wait
		nil,       // arguments
	)
	FailOnError(err, "Failed to declare a queue")
	//manipulate value request
	bodyReq := Dto.ReqPG{
		AccountNo:       "5252522525252",
		Amount:          100000,
		SourceAccountNo: "1231313123213",
	}
	dataBytes, _ := json.Marshal(bodyReq)

	q, err := ch.QueueDeclare(
		"rpc_name",    // name
		true, // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	FailOnError(err, "Failed to declare a queue")
	corrId := "Jefry"
	err = ch.Publish(
		"payment",             // exchange
		"duit.transfer", // routing key
		false,                 // mandatory
		false,                 // immediate
		amqp.Publishing{
			Headers:       nil,
			ContentType:   "text/plain",
			CorrelationId: corrId,
			ReplyTo:       q.Name,
			Body:          dataBytes,
		})
	FailOnError(err, "Failed to publish a message")
	log.Printf(" [x] Sent %s\n", string(dataBytes))

	msgs, err := ch.Consume(
		q.Name, // queue
		"",     // consumer
		true,   // auto-ack
		false,  // exclusive
		false,  // no-local
		false,  // no-wait
		nil,    // args
	)

	for d := range msgs {
		if d.CorrelationId == corrId {
			log.Printf("Received a message: %s", d.Body)
			break
		}
	}

	log.Printf(" [*] Waiting for messages. To exit press CTRL+C")

}
